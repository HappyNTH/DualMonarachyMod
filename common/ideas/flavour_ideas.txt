#################################################
## Ideas File For All Flavour National Spirits ##
##              Coded by HappyNTH              ##
#################################################
ideas = {
	country = {
		###########
		# MIS / GPF
		###########
		treaty_of_fayetteville = {
			picture = generic_neutrality_drift_bonus
			removal_cost = -1
			allowed = {
				always = no
			}
			modifier = {
				custom_modifier_tooltip = treaty_of_fayetteville_tt
			}
		}
	}
}