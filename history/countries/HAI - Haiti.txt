﻿capital = 318

oob = "HAI_1936"

# Starting tech
set_technology = {
	infantry_weapons = 1
	infantry_weapons1 = 1
	tech_support = 1
	tech_engineers = 1
	tech_recon = 1
	gw_artillery = 1
	gwtank = 1
	basic_light_tank = 1
	early_fighter = 1
	naval_bomber1 = 1
	early_bomber = 1
	trench_warfare = 1
	fleet_in_being = 1
}
set_country_flag = monroe_doctrine
set_cosmetic_tag = HAI_SPA
set_war_support = 0.1
set_convoys = 5

set_politics = {
	ruling_party = democratic
	last_election = "1936.1.1"
	election_frequency = 48
	elections_allowed = no
}
set_popularities = {
	democratic = 100
}

create_country_leader = {
	name = "Sténio Vincent"
	desc = "POLITICS_STENIO_VINCENT_DESC"
	picture = "GFX_portrait_haiti_stenio_vincent"
	expire = "1965.1.1"
	ideology = conservatism
	traits = {
		#
	}
}

create_country_leader = {
	name = "Jacques Roumain"
	desc = "POLITICS_JACQUES_ROUMAIN_DESC"
	picture = "gfx/leaders/South America/Portrait_South_America_Generic_1.dds"
	expire = "1965.1.1"
	ideology = marxism
	traits = {
		#
	}
}
