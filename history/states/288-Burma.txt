
state={
	id=288
	name="STATE_288"
	manpower = 5265901
	
	state_category = town
	
	history={
		add_core_of = BRM
		owner = SIA
		victory_points = {
			1330 5 
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 1
			1330 = {
				naval_base = 4
			}
		}

	}

	provinces={
		1296 1315 1330 1385 1403 1417 1650 4202 4275 4293 4336 4408 4626 7034 7295 7441 10154 10217 10283 10344 12125 12127 12238 12256 12292 12449 
	}
}
