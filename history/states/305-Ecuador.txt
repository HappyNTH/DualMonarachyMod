
state={
	id=305
	name="STATE_305"
	manpower = 2000000
	resources={
		oil=2 # was: 2
	}
	
	state_category = large_city

	history={
		owner = USA
		buildings = {
			infrastructure = 5
			arms_factory = 2
			industrial_complex = 5
			air_base = 5
			8252 = {
				naval_base = 5
			}
		}
		add_core_of = USA
		victory_points = {
			12798 5
		}
		victory_points = {
			8252 1
		}

	}

	provinces={
		5051 5120 8047 8075 8252 10888 11020 12798 
	}
}
