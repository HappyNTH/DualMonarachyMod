
state={
	id=372
	name="STATE_372"
	manpower = 1854481

	state_category = town

	resources={
		aluminium=14 # was: 20
	}


	history={
		owner = MIS
		buildings = {
			infrastructure = 4
			industrial_complex = 2
		}
		add_core_of = MIS
		victory_points = {
			7834 1
		}
		victory_points = {
			10717 10
		}
		victory_points = {
			1806 1
		}
		set_demilitarized_zone = yes
	}

	provinces={
		7892 1352 7834 9802 2043 4811 1806 4567 1534 1564 10717
	}
}
