
state={
	id=375
	name="STATE_375"
	manpower = 5824712


	state_category = metropolis

	resources={
		oil=320 # was: 580
		aluminium=50 # was: 96
	}


	history={
		owner = MIS
		buildings = {
			infrastructure = 6
			industrial_complex = 9
			air_base = 3
			arms_factory = 1
			10337 = {
				naval_base = 3
			}
		}
		add_core_of = MIS
		victory_points = {
			10337 10
		}
		victory_points = {
			3960 10
		}
		victory_points = {
			4740 3
		}
	}

	provinces={
		805 1500 1915 2082 3875 3960 4577 5001 5022 6968 7628 7836 7981 9829 9920 10337 10504 10778 10861 12341 12410 12486 12875 1618 4919 5103 6803 7762 7904 7945 8025 9822 10702 10798 11743 11802 5085 10340 4740
	}
}
