
state={
	id=499
	name="STATE_499"
	manpower = 4233900
	
	state_category = city

	history={
		owner = USA
		buildings = {
			infrastructure = 5
			industrial_complex = 3
			dockyard = 1
			air_base = 1
			10946 = {
				naval_base = 1
			}
		}
		add_core_of = USA
		victory_points = {
			10946 1 
		}

	}

	provinces={
		4617 5194 8143 8177 8184 8197 8242 10392 10926 10946 5174 8129 5218 12422 10419 11002
	}
}
