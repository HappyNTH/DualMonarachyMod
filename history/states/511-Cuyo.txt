
state={
	id=511
	name="STATE_511" # Mendoza
	manpower = 1090274
	
	state_category = town

	history={
		owner = USA
		buildings = {
			infrastructure = 5
			industrial_complex = 3
			air_base = 4
		}
		add_core_of = USA
		victory_points = {
			12942 1 
		}

	}

	provinces={
		2158 2179 2215 5152 5184 5219 7550 8215 10964 10992 11021 11022 12942 12970 
	}
}
