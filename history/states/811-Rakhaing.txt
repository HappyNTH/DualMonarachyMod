
state={
	id=811
	name="STATE_811"
	manpower = 5265901
	
	state_category = town
	
	resources={
		rubber=3 # was: 4
		tungsten = 92
	}

	history={
		add_core_of = BRM
		add_core_of = RAJ
		owner = RAJ
		victory_points = {
			12090 1 
		}
		buildings = {
			infrastructure = 4
			industrial_complex = 1
			air_base = 1
			12090 = {
				naval_base = 1
			}
		}

	}

	provinces={
		4454 3999 7534 11966 7163 10033 7122 1142 4147 12090 4541 7190 4019 1014 4059 1333 12284 12197 10267 3651 10182 7254 10223 7382 12267
	}
}
