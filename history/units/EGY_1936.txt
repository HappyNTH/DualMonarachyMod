﻿
division_template = {
	name = "Al-Mushati Garrison"
	division_names_group = EGY_GAR_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 0 y = 3 }
		infantry = { x = 0 y = 4 }
	}
}
division_template = {
	name = "Al-Mushati Infantry"
	division_names_group = EGY_INF_01
	regiments = {
		infantry = { x = 0 y = 0 }
		infantry = { x = 0 y = 1 }
		infantry = { x = 0 y = 2 }
		infantry = { x = 1 y = 0 }
		infantry = { x = 1 y = 1 }
		infantry = { x = 1 y = 2 }
		infantry = { x = 2 y = 0 }
		infantry = { x = 2 y = 1 }
		infantry = { x = 2 y = 2 }
	}
}
division_template = {
	name = "Sipahis Cavalry"
	division_names_group = EGY_CAV_01
	regiments = {
		cavalry = { x = 0 y = 0 }
		cavalry = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
	}
}
division_template = {
	name = "Camel Corps"
	division_names_group = EGY_CAV_01
	regiments = {
		camelry = { x = 0 y = 0 }
		camelry = { x = 0 y = 1 }
		camelry = { x = 0 y = 2 }
		camelry = { x = 1 y = 0 }
		camelry = { x = 1 y = 1 }
		camelry = { x = 1 y = 2 }
	}
}
division_template = {
	name = "Mountain Division" # Mountain Brigade

	division_names_group = EGY_MTN_01

	regiments = {
		mountaineers = { x = 0 y = 0 }
		mountaineers = { x = 0 y = 1 }
		mountaineers = { x = 0 y = 2 }
		mountaineers = { x = 1 y = 0 }
		mountaineers = { x = 1 y = 1 }
		mountaineers = { x = 1 y = 2 }
	}
}
division_template = {
	name = "Armoured Divisions" # Armour Division

	division_names_group = EGY_ARM_01

	regiments = {
		light_armor = { x = 0 y = 0 }
		light_armor = { x = 0 y = 1 }
		cavalry = { x = 0 y = 2 }
		cavalry = { x = 0 y = 3 }
		cavalry = { x = 1 y = 0 }
		cavalry = { x = 1 y = 1 }
		cavalry = { x = 1 y = 2 }
		cavalry = { x = 1 y = 3 }
	}
	support = {
		recon = { x = 0 y = 0 }
	}
}

units = {
	######################
	###	TURKISH TROOPS ###
	######################
	division = {
		#name = "14. Sipahis Cavalry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 14
		}
		location = 11811
		division_template = "Sipahis Cavalry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	## VI. Kolordu ##
	division = {
		#name = "7. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 7
		}
		location = 11811
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "17. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 17
		}
		location = 11811
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "20. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 20
		}
		location = 11811
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}

	### Birinci Ordu ###
	division = {
		#name = "11. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 11
		}
		location = 10122
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "2. Sipahis Cavalry (armored)"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 2
		}
		location = 7454
		division_template = "Armoured Divisions" 
		start_experience_factor = 0.1
		start_equipment_factor = 0.8

	}
	## III. Kolordu ##
	division = {
		#name = "1. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 1005
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "23. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 23
		}
		location = 7454
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "24. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 24
		}
		location = 9893
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	## IV. Kolordu ##
	division = {
		#name = "8. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 8
		}
		location = 9893
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "22. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 22
		}
		location = 9932
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "1. Mountain Division"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 12476
		division_template = "Mountain Division" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}

	### Ikinci Ordu Ordu ###
	division = {
		#name = "39. Mountain Division"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 39
		}
		location = 947
		division_template = "Mountain Division" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	## I. Kolordu ##
	division = {
		#name = "6. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 6
		}
		location = 4112
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "16. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 16
		}
		location = 4112
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "Çanakkale Al-Mushati Garrison"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 14
		}
		location = 4112
		division_template = "Al-Mushati Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "EGY" } }
		start_experience_factor = 0.5
		start_equipment_factor = 0.3

	}
	## II.  Kolordu ##
	division = {
		#name = "4. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 4
		}
		location = 849
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "18. Mountain Division"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 18
		}
		location = 849
		division_template = "Mountain Division" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	## V. Kolordu ##
	division = {
		#name = "5. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 5
		}
		location = 947
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "25. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 25
		}
		location = 947
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "Hakkâri Al-Mushati Garrison"
		division_name = {
			is_name_ordered = yes
			name_order = 82
		}
		location = 12476
		division_template = "Al-Mushati Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "EGY" } }
		start_experience_factor = 0.3
		start_equipment_factor = 0.1

	}
	division = {
		#name = "Van Al-Mushati Garrison"
		division_name = {
			is_name_ordered = yes
			name_order = 83
		}
		location = 6935
		division_template = "Al-Mushati Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "EGY" } }
		start_experience_factor = 0.3
		start_equipment_factor = 0.1

	}
	division = {
		#name = "Diyarbakır Al-Mushati Garrison"
		division_name = {
			is_name_ordered = yes
			name_order = 84
		}
		location = 4512
		division_template = "Al-Mushati Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "EGY" } }
		start_experience_factor = 0.3
		start_equipment_factor = 0.1

	}
	division = {
		#name = "Erzurum Al-Mushati Garrison"
		division_name = {
			is_name_ordered = yes
			name_order = 85
		}
		location = 11853
		division_template = "Al-Mushati Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "EGY" } }
		start_experience_factor = 0.3
		start_equipment_factor = 0.1

	}

	### Üçüncüsü Ordu ###
	## VII. Kolordu ##
	division = {
		#name = "2. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 2
		}
		location = 4512
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "10. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 10
		}
		location = 4512
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	## VIII. Kolordu ##
	division = {
		#name = "12. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 12
		}
		location = 11881
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "15. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 15
		}
		location = 11881
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	## IX. Kolordu ##
	division = {
		#name = "3. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 3
		}
		location = 10403
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "9. Al-Mushati Infantry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 9
		}
		location = 10472
		division_template = "Al-Mushati Infantry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "1. Sipahis Cavalry"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 1
		}
		location = 10472
		division_template = "Sipahis Cavalry" 
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	##### Royal Iraqi Army #####
	division = {
		#name = "1 Alfurqat"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 13
		}
		location = 2097
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		#name = "2 Alfurqat"
		division_name = {    # New syntax for compatibility with name lists
			is_name_ordered = yes
			name_order = 19
		}
		location = 10106
		division_template = "Mountain Division"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	

	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 26
		}
		location = 7011
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.5
		start_equipment_factor = 1

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 27
		}
		location = 1155
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 28
		}
		location = 4206
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 29
		}
		location = 4076
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 30
		}
		location = 792
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 31
		}
		location = 4111
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 32
		}
		location = 11976
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 33
		}
		location = 5037
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 34
		}
		location = 10840
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 35
		}
		location = 10760
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 36
		}
		location = 8051
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 37
		}
		location = 12806
		division_template = "Al-Mushati Infantry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}


	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 3
		}
		location = 9989
		division_template = "Sipahis Cavalry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 4
		}
		location = 12727
		division_template = "Sipahis Cavalry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}
	division = {
		division_name = {
			is_name_ordered = yes
			name_order = 5
		}
		location = 12744
		division_template = "Sipahis Cavalry"	
		start_experience_factor = 0.2
		start_equipment_factor = 0.5

	}

	
	###Malaya
	division= {	
		name = "Singapore Fortress"
		location = 12299
		division_template = "Al-Mushati Garrison"
		start_experience_factor = 0.2
		start_equipment_factor = 0.8

	}
	division= {	
		name = "Penang Fortress"
		location = 12215
		division_template = "Al-Mushati Garrison"
		start_experience_factor = 0.2
		start_equipment_factor = 0.15

	}
	###Ceylon
	division= {	
		name = "Ceylon Defense Force"
		location = 7260
		division_template = "Al-Mushati Garrison"
		force_equipment_variants = { infantry_equipment_0 = { owner = "EGY" } }
		start_experience_factor = 0.1
		start_equipment_factor = 0.15

	}

}

air_wings = {
	49 = { 
		fighter_equipment_0 = {
			owner = "EGY" 
			amount = 45
		}
		CAS_equipment_1 = {
			owner = "EGY" 
			amount = 16
		}
	}
	447 = { 
		fighter_equipment_0 = {
			owner = "EGY" 
			amount = 50
		}
		CAS_equipment_1 = {
			owner = "EGY" 
			amount = 10
		}
	}
	291 = { 
		fighter_equipment_0 = {
			owner = "EGY" 
			amount = 25
		}
	}
	336 = { 
		fighter_equipment_0 = {
			owner = "EGY" 
			amount = 12
		}
	}
}


instant_effect = {
	add_equipment_production = {
		equipment = {
			type = infantry_equipment_1
			creator = "EGY"
		}
		requested_factories = 1
		progress = 0.43
		efficiency = 100
	}
	add_equipment_production = {
		equipment = {
			type = fighter_equipment_0
			creator = "EGY"
		}
		requested_factories = 2
		progress = 0.47
		efficiency = 100
	}
}