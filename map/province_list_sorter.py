import pyperclip

def order():
    province_list = input("Paste raw list of provinces here : ").split(' ') # Turn the raw string into a string
    province_list = list(dict.fromkeys(province_list)) # Remove duplicates by changing to a dict & back
    province_list.sort() # Sort the list into numerical order
    output_list = ' '.join(map(str, province_list))
    print('\nOutput Copied to Clipboard')
    pyperclip.copy(output_list)

def remove_values():
    input_list = input("Paste raw list of provinces here : ").split(' ') # Turn the raw string into a string
    input_list = list(dict.fromkeys(input_list)) # Remove duplicates by changing to a dict & back
    input_list.sort() # Sort the list into numerical order
    remove_list = input("Paste raw list of provinces *to be removed* here : ").split()
    remove_list = list(dict.fromkeys(remove_list))
    remove_list.sort()
    output_list = [x for x in input_list if x not in remove_list]
    output_list = ' '.join(map(str, output_list))
    print('\nOutput Copied to Clipboard')
    pyperclip.copy(output_list)

def init():
    use_input = ""
    while use_input != "1" and use_input != "2":
        use_input = input("Reorder & Remove Duplicates (1) / Remove Values from List (2) : ")
    if use_input == "1":
        order()
    else:
        remove_values()

init()